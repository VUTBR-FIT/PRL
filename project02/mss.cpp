/* 
 * File:   mcc.cpp
 * Author: Lukáš Černý (xcerny63)
 *
 * Created on April 1, 2018, 12:27 AM
 */

#include <mpi.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>

#define TAG 0
#define FILE_INPUT "numbers"
#ifdef EN_DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif

using namespace std;
/**
 * Funkce ktera posle cisla od procesoru form a vrti je pres promenou numvers
 * @param numbers vektor prijatych cise
 * @param from Id procesuru od ktereho prijima cisla
 * @param to Id procesu ktery prijima cisla
 */
void recv(vector<int> *numbers, int from, int to) {
    int number;
    MPI_Status stat;
    (*numbers).clear();
    
    do {
        MPI_Recv(&number, 1, MPI_INT, from, TAG, MPI_COMM_WORLD, &stat);
        if (number != -1) (*numbers).push_back(number);
    } while (number != -1);
}
/**
 * Posle cisla obsazena v promene numvers procesoru s id t
 * @param numbers Cisla k odeslani
 * @param from Id procesoru posilajici cisl
 * @param to Id prijimaciho proceaor
 */
void send(vector<int> *numbers, int from, int to) {
    int number;
    for (int i = 0; i < (*numbers).size(); i++) {
        number = (*numbers)[i];
        MPI_Send(&number, 1, MPI_INT, to, TAG, MPI_COMM_WORLD);
    }
    number = -1;
    MPI_Send(&number, 1, MPI_INT, to, TAG, MPI_COMM_WORLD);
}
/**
 * Spoji dva vektory cisel, seradi je a pak ve spravnem pomeru vrti zoet do puvodnich vektor
 * @param left Levy seznam cise
 * @param right Pravy seznam cise
 */
void sortVectors(vector<int> *left, vector<int> *right) {
    vector<int> temp;
    int leftSize = (*left).size();
    
    temp.reserve((*left).size() + (*right).size());
    temp.insert(temp.begin(), (*left).begin(), (*left).end());
    temp.insert(temp.begin(), (*right).begin(), (*right).end());
    
    sort(temp.begin(), temp.end());

    (*left).clear();
    (*right).clear();
    for (int i = 0; i < temp.size(); i++) {
        if (i < leftSize) (*left).push_back(temp[i]);
        else (*right).push_back(temp[i]);
    }
}
/**
 * algoritmus merge-splitting sor
 * @param myId Id soucasneho procesor
 * @param processorCount Pocet procesoru
 */
void sorting(int myId, int procesorCount) {
    int number;
    vector<int> numbers;
    MPI_Status stat;

    do {
        MPI_Recv(&number, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);
        if (number != -1) numbers.push_back(number);
    } while (number != -1);
    
    sort(numbers.begin(), numbers.end());

    int numCycles = (procesorCount + (procesorCount % 2))/ 2;
    int oddLimit = procesorCount - (procesorCount % 2);
    int evenLimit = procesorCount - 1 + (procesorCount % 2);
    
    for (int i = 0; i < numCycles; i++) {
        if (myId % 2 == 0 && myId < oddLimit) {
            send(&numbers, myId, myId + 1);
            recv(&numbers, myId + 1, myId);
        } else if (myId % 2 == 1 && myId <= oddLimit) {
            vector<int> temp;
            recv(&temp, myId - 1, myId);
            sortVectors(&temp, &numbers);
            send(&temp, myId, myId - 1);
        }
        if (myId % 2 == 1 && myId < evenLimit) {
            send(&numbers, myId, myId + 1);
            recv(&numbers, myId + 1, myId);
        } else if (myId % 2 == 0 && myId <= evenLimit && myId != 0) {
            vector<int> temp;
            recv(&temp, myId - 1, myId);
            sortVectors(&temp, &numbers);
            send(&temp, myId, myId - 1);
        }
    }
    
    send(&numbers, myId, 0);
}
/**
 * Telo procesoru s Id == 0. Nacte cisla ze souboru a rozesle je vsem proceosrum (i sobe)
 * @param count Pocet procesor
 */
void mainProcesor(int count) {
    bool first = true;
    int number, procesorId = 0;
    fstream file;
    file.open(FILE_INPUT, ios::in);
    // cteni souboru a posilani cisel
    while (file.good()) {
        number = file.get();
        if (!file.good()) break;
        if (!DEBUG) cout << (first ? "" : " ") << number;
        first = false;
        MPI_Send(&number, 1, MPI_INT, procesorId, TAG, MPI_COMM_WORLD);
        
        procesorId = (procesorId + 1) % count;
    }
    if (!DEBUG) cout << endl;
    file.close();

    number = -1;
    for (int id = 0; id < count; id++)
        MPI_Send(&number, 1, MPI_INT, id, TAG, MPI_COMM_WORLD);
}
/**
 * Prijme serazena cisla od vsech procesoru a vytiskne je od nejmensiho Id procesoru
 * Je volana pouze procesorem s Id == 0
 * @param procesorCount pocet procesoru
 */
void print(int procesorCount) {
    for (int i = 0; i < procesorCount; i++) {
        vector<int> temp;
        recv(&temp,  i, 0);
        for (int j = 0; j < temp.size(); j++)
            cout << temp[j] << endl;
    }
}

int main(int argc, char** argv) {
    int procesorCount; // pocet procesoru
    int myId; // rank procesoru

    // MPI INIT
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procesorCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    double startTime = MPI_Wtime();

    if (myId == 0)
        mainProcesor(procesorCount);
    
    sorting(myId, procesorCount);
    
    if (myId == 0 && !DEBUG) print(procesorCount);
    
    if (myId == 0 && DEBUG) cout << procesorCount << ":" << MPI_Wtime() - startTime << endl;
    
    MPI_Finalize();
    return 0;
}

