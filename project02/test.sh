#!/bin/bash

srcData="mss.cpp"
name="mss"
logFile="output.log"

function runner {
    count=$1
    echo "${count}" >> ${logFile}
    ( dd if=/dev/urandom bs=1 count="${count}" of=numbers 2>> /dev/null )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 1 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 5 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 10 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 13 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 20 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 42 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 50 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 80 ${name} >> ${logFile} )
    ( mpirun --prefix /usr/local/share/OpenMPI -np 100 ${name} >> ${logFile} )
}

if [ $# -ne 2 ]; then
    rm ${logFile} 2>> /dev/null
    # Preklad
    mpic++ --prefix /usr/local/share/OpenMPI -DEN_DEBUG -o ${name} ${srcData}
    runner 100
    runner 200
    runner 400
    runner 800
    runner 1600
else
    # Preklad
    mpic++ --prefix /usr/local/share/OpenMPI -o ${name} ${srcData}
    # Vygenerovani souboru s nahodnymi cisly
    dd if=/dev/random bs=1 count="$1" of=numbers 2>> /dev/null
    # Spusteni
    ( mpirun --prefix /usr/local/share/OpenMPI -np "$2" ${name} )
fi;

#uklid
rm -f ${name} numbers
