/* 
 * File:   pro.h
 * Author: user
 *
 * Created on April 28, 2018, 3:31 AM
 */

#include <stdlib.h>
#include <vector>

using namespace std;


#ifndef PRO_H
#define PRO_H

#define TAG 0

struct edge {
    int from;
    int to;
};
int myId; // rank procesoru
vector<vector<struct edge> *> matrix;
vector<struct edge> edgeList;
/**
 * Funkce ktera posle cisla od procesoru form a vrati je pres promenou numvers
 * @param numbers vektor prijatych cise
 * @param from Id procesuru od ktereho prijima cisla
 * @param to Id procesu ktery prijima cisla
 */
void recv(vector<int> *numbers, int from, int to);
/**
 * Posle cisla obsazena v promene numvers procesoru s id t
 * @param numbers Cisla k odeslani
 * @param from Id procesoru posilajici cisl
 * @param to Id prijimaciho proceaor
 */
void send(vector<int> *numbers, int from, int to);

unsigned int matrixGetIndex(unsigned int row);
struct edge matrixNextEdge(int start, int end);
void metrixGetLast(unsigned int row, struct edge *result);
void matrixAddEdge(unsigned int row, struct edge inserted);
void initEdge(struct edge *edge, unsigned int form, unsigned int to);
void matrixInit(int count);

void initEdgeList(struct edge *edge);

bool getLast(vector<struct edge *> *source, unsigned int index, struct edge *last);

void createMatrix(unsigned int count);

#endif /* PRO_H */

