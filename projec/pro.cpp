/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on April 27, 2018, 3:53 PM
 */

#ifdef EN_DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif

#include <mpi.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include "pro.h"

using namespace std;

void printEdge(struct edge *edge) {
    cout << "(" << edge->from << "," << edge->to << ")";
}

void printEdgeList(vector<struct edge> *list) {
    for (int i = 0; i < list->size(); i++) {
        printEdge(&(list->at(i)));
        cout << "\t";
    }
}

void matrixPrint() {
    for (int i = 0; i < matrix.size(); i++) {
        printEdgeList(matrix[i]);
        cout << "\n";
    }
}

/*
/*
 * 
 *
int mainProcesor(int count) {
    vector<char> nodes;


    return 0;
}

void print(int procesorCount) {
    for (int i = 0; i < procesorCount; i++) {
        vector<int> temp;
        recv(&temp, i, 0);
        for (int j = 0; j < temp.size(); j++)
            cout << temp[j] << endl;
    }
}*/
vector<struct edge> synchEtour(int procesorId) {
    vector<int> temp1;
    struct edge temp;
    vector<struct edge> etour;
    if (procesorId == 0) {// MAIN_PROCESOR
        temp = matrixNextEdge(edgeList.at(myId).from, edgeList.at(myId).to);
        etour.push_back(temp);
        for (int i = 1; i < edgeList.size(); i++) {
            temp1.clear();
            recv(&temp1, i, 0);
            initEdge(&temp, temp1.at(0), temp1.at(1));
            etour.push_back(temp);
        }
        for (int i = 0; i < etour.size(); i++) {
            temp1.clear();
            temp = etour.at(i);
            temp1.push_back(temp.from);
            temp1.push_back(temp.to);
            for (int p = 1; p < etour.size(); p++) {
                send(&temp1, myId, p);
            }
        }
    } else {
        temp = matrixNextEdge(edgeList.at(myId).from, edgeList.at(myId).to);
        temp1.push_back(temp.from);
        temp1.push_back(temp.to);
        send(&temp1, myId, 0);
        for (int i = 0; i < edgeList.size(); i++) {
            temp1.clear();
            recv(&temp1, 0, myId);
            initEdge(&temp, temp1.at(0), temp1.at(1));
            etour.push_back(temp);
        }
    }
    return etour;
}

int findEdgePositionInList(vector<struct edge> source, int from, int to) {
    struct edge temp;
    for (int i = 0; i < source.size(); i++) {
        temp = source.at(i);
        if (temp.from == from && temp.to == to)
            return i;
    }
    return -1;
}

vector<int> orderInEtour(int procesorId, vector<struct edge> etour) {
    vector<int> result;
    int pos;
    for (int i = 0; i < edgeList.size(); i++)
        result.push_back(0);
    struct edge temp = edgeList.at(0);
    for (int i = 1; i <= etour.size(); i++) {
        pos = findEdgePositionInList(edgeList, temp.from, temp.to);
        result[pos] = i;
        temp = etour.at(pos);
    }
    return result;
}

bool isForward(vector<int> orderInEtour, int from, int to) {
    int pos = findEdgePositionInList(edgeList, from, to);
    int rPos = findEdgePositionInList(edgeList, to, from);
    return orderInEtour.at(pos) < orderInEtour.at(rPos);
}

vector<int> suffixSums(vector<struct edge> etour, vector<int> weight) {
    struct edge myEdge = edgeList.at(myId);
    vector<int> suffix;
    printEdge(&myEdge);
    cout << endl;
    /*/
    //int myPos = 
    int posSucc = findEdgePositionInList(etour, )
    struct edge successor = 
    struct edge predecessor
    
    struct edge temp;
    int pos, max = 0, tempSum = 0;

    for (int i = 0; i < edgeList.size(); i++) {
        suffix.push_back(0);
        if (max < orderInEtour.at(i)) {
            max = orderInEtour.at(i);
            temp = edgeList.at(i);
        }
    }

    for (int i = 0; i < etour.size(); i++) {
        pos = findEdgePositionInList(edgeList, temp.from, temp.to);
        tempSum += weight.at(pos);
        suffix[pos] = tempSum;
        pos = findEdgePositionInList(etour, temp.from, temp.to);
        temp = edgeList.at(pos);
    }*/
    return suffix;
}

vector<struct edge>initRoot(vector<struct edge> list) {
    vector<struct edge> *temp = matrix.at(0);
    
    struct edge last = temp->back();
    int pos = findEdgePositionInList(list, last.to, last.from);
    struct edge modificated = list.at(pos);
    modificated.from = last.from;
    list[pos] = modificated;
    pos = findEdgePositionInList(edgeList, last.to, last.from);
    edgeList[pos] = modificated;
    return list;
}

int main(int argc, char** argv) {
    int procesorCount; // pocet procesoru

    // MPI INIT
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procesorCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    double startTime = MPI_Wtime();

    if (procesorCount == 1) {
        cout << argv[1] << endl;
    } else {
        matrixInit((procesorCount + 2) / 2);
        vector<struct edge> etour = synchEtour(myId);
        etour = initRoot(etour);
        vector<int> posiList = orderInEtour(myId, etour);
        vector<int> weight;
        struct edge temp;
        for (int i = 0; i < edgeList.size(); i++) {
            temp = edgeList.at(i);
            if (isForward(posiList, temp.from, temp.to))
                weight.push_back(1);
            else weight.push_back(0);
        }
        //vector<int> suffixSum = suffixSums(etour, weight);
        if (myId == 0) {
            cout << endl;
            printEdgeList(&etour);
            /*cout << endl;
            printEdgeList(&etour);
            cout << endl;
            /*for (int i = 0; i < suffixSum.size(); i++)
                cout << i << ":" << posiList.at(i) << "\t";
            cout << endl;
            for (int i = 0; i < suffixSum.size(); i++)
                cout << i << ":" << weight.at(i) << "\t";
            cout << endl;
            for (int i = 0; i < suffixSum.size(); i++)
                cout << i << ":" << suffixSum.at(i) << "\t";
            cout << endl << isForward(posiList, 1, 2);
            cout << endl; //*/
        }
        //printEdge(&temp);
        //temp = matrixNextEdge(1, 2);

        //printEdge(&temp);
    }
    if (myId == 0 && DEBUG) cout << procesorCount << ":" << MPI_Wtime() - startTime << endl;

    MPI_Finalize();
    return 0;
}

void initEdge(struct edge *edge, unsigned int from, unsigned int to) {
    edge->from = from;
    edge->to = to;
}

void matrixAddEdge(unsigned int row, struct edge inserted) {
    if (matrix.size() >= row)
        matrix.at(matrixGetIndex(row))->push_back(inserted);
}

unsigned int matrixGetIndex(unsigned int row) {
    return row - 1;
}

struct edge matrixNextEdge(int start, int end) {
    vector<struct edge> *list = matrix.at(matrixGetIndex(end));
    struct edge temp;
    int found = 0;
    for (int i = 0; i < list->size(); i++) {
        temp = list->at(i);
        if (found == 1) {
            found++;
            break;
        }

        if (temp.to != start)
            continue;
        found = 1;
    }

    if (found == 2) return temp;
    else return list->at(0);
}

void matrixInit(int count) {
    for (int i = 0; i < count; i++)
        matrix.push_back(new vector<struct edge>);
    struct edge temp;
    for (int i = 1; i <= count; i++) {
        if (2 * i <= count) {
            initEdge(&temp, i, 2 * i);
            edgeList.push_back(temp);
            matrixAddEdge(i, temp);
        }
        if (2 * i + 1 <= count) {
            initEdge(&temp, i, 2 * i + 1);
            edgeList.push_back(temp);
            matrixAddEdge(i, temp);
        }
        if (i % 2 == 0 && ((int) i / 2) > 0) {
            initEdge(&temp, i, i / 2);
            edgeList.push_back(temp);
            matrixAddEdge(i, temp);
        } else if (i % 2 == 1 && ((int) ((i - 1) / 2))) {
            initEdge(&temp, i, (i - 1) / 2);
            edgeList.push_back(temp);
            matrixAddEdge(i, temp);
        }
    }
}

void recv(vector<int> *numbers, int from, int to) {
    int number;
    MPI_Status stat;
    (*numbers).clear();

    do {
        MPI_Recv(&number, 1, MPI_INT, from, TAG, MPI_COMM_WORLD, &stat);
        if (number != -1) (*numbers).push_back(number);
    } while (number != -1);
}

void send(vector<int> *numbers, int from, int to) {
    int number;
    for (int i = 0; i < (*numbers).size(); i++) {
        number = (*numbers)[i];
        MPI_Send(&number, 1, MPI_INT, to, TAG, MPI_COMM_WORLD);
    }
    number = -1;
    MPI_Send(&number, 1, MPI_INT, to, TAG, MPI_COMM_WORLD);
}
