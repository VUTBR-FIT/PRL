#!/bin/bash
srcData="pro.cpp"
name="pro"

if [ $# -eq 1 ]; then
    # Preklad
    mpic++ --prefix /usr/local/share/OpenMPI -std=c++11 -o ${name} ${srcData}
    # Spusteni
    length=$((${#1}*2-2))
    mpirun --prefix /usr/local/share/OpenMPI -np ${length} ${name} "$1"
fi;
